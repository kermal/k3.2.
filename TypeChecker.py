#!/usr/bin/python
import AST
from SymbolTable import SymbolTable, VariableSymbol, FunctionSymbol


ttype = {
    '+': {
        'int': {
            'int': 'int',
            'float': 'float',
        },
        'float': {
            'int': 'float',
            'float': 'float',
        }
    },
    '-': {
        'int': {
            'int': 'int',
            'float': 'float',
        },
        'float': {
            'int': 'float',
            'float': 'float',
        }
    },
    '*': {
        'int': {
            'int': 'int',
            'float': 'float',
        },
        'float': {
            'int': 'float',
            'float': 'float',
        },
        'string': {
            'int': 'string'
        }
    },
    '/': {
        'int': {
            'int': 'float',
            'float': 'float',
        },
        'float': {
            'int': 'float',
            'float': 'float',
        },
    },
    '>': {
        'int': {
            'int': 'int',
            'float': 'int',
        },
        'float': {
            'int': 'int',
            'float': 'int',
        },
        'string': {
            'string': 'int'
        }
    },
    '>=': {
        'int': {
            'int': 'int',
            'float': 'int',
        },
        'float': {
            'int': 'int',
            'float': 'int',
        },
        'string': {
            'string': 'int'
        }
    },
    '<': {
        'int': {
            'int': 'int',
            'float': 'int',
        },
        'float': {
            'int': 'int',
            'float': 'int',
        },
        'string': {
            'string': 'int'
        }
    },
    '<=': {
        'int': {
            'int': 'int',
            'float': 'int',
        },
        'float': {
            'int': 'int',
            'float': 'int',
        },
        'string': {
            'string': 'int'
        }
    },
    '!=': {
        'int': {
            'int': 'int',
            'float': 'int'
        },
        'float': {
            'int': 'int',
            'float': 'int'
        },
        'string': {
            'string': 'int'
        }
    },

}

symbolTable = SymbolTable(None, "pierwszy_scope")


def printError(lineNumber, string):
    print "Line %s:\t %s" % (lineNumber, string)


class NodeVisitor(object):
    def visit(self, node):
        method = 'visit_' + node.__class__.__name__
        visitor = getattr(self, method, self.generic_visit)
        return visitor(node)

    def generic_visit(self, node):        # Called if no explicit visitor function exists for a node.
        if isinstance(node, list):
            for elem in node:
                self.visit(elem)
        elif node:
            for child in node.children:
                if isinstance(child, list):
                    for item in child:
                        if isinstance(item, AST.Node):
                            self.visit(item)
                elif isinstance(child, AST.Node):
                    self.visit(child)

    # simpler version of generic_visit, not so general
    #def generic_visit(self, node):
    #    for child in node.children:
    #        self.visit(child)


class TypeChecker(NodeVisitor):
    def visit_Program(self, node):
        #TODO self.visit(node.children)
        self.visit(node.declarations)
        self.visit(node.fundefs)
        self.visit(node.instructions)

    def visit_Declaration(self, node):
        for init in node.inits.list:
            visited = self.visit(init.expression)
            if visited != node.type:
                errorString = "'%s' is type '%s', but expression is '%s'" % (str(init.id), str(node.type), visited)
                printError(init.lineno, errorString)
            if symbolTable.put(str(init.id), VariableSymbol(init.id, node.type)):
                errorString = "redeclaration of '%s'" % (init.id, )
                printError(init.lineno, errorString)

    def visit_PrintInstruction(self, node):
        expressionType = self.visit(node.expression)
        if not expressionType:
            errorString = "undeclared '%s'" % (node.expression, )
            printError(node.lineno, errorString)

    def visit_LabeledInstruction(self, node):
        self.visit(node.instruction)

    def visit_Assignment(self, node):
        if symbolTable.get(node.id.const):
            variableType = symbolTable.get(node.id.const).type
        else:
            errorString = "undeclared '%s'" % (node.id, )
            printError(node.lineno, errorString)
            return

        visited = self.visit(node.expression)
        if variableType != visited:
            errorString = "problem assignment '%s' into '%s'" % (visited, variableType)
            printError(node.lineno, errorString)

    def visit_ChoiceInstructionIfElse(self, node):
        if self.visit(node.condition) != 'int':
            printError(node.lineno, "statement in IF isn't integer")
        self.visit(node.if_instruction)
        self.visit(node.else_instruction)

    def visit_ChoiceInstructionIf(self, node):
        if self.visit(node.condition) != 'int':
            printError(node.lineno, "statement in IF isn't integer")
        self.visit(node.if_instruction)

    def visit_WhileInstruction(self, node):
        if self.visit(node.condition) != 'int':
            printError(node.lineno, "statement in WHILE isn't integer")
        self.visit(node.instruction)

    def visit_RepeatInstruction(self, node):
        if self.visit(node.condition) != 'int':
            printError(node.lineno, "statement in REPEAT isn't integer")
        self.visit(node.instructions)

    def visit_ReturnInstruction(self, node):
        self.visit(node.expression)

    def visit_CompoundInstruction(self, node):
        self.visit(node.declarations)
        self.visit(node.instructions)

    def visit_Condition(self, node):
        return self.visit(node.expression)

    def visit_Integer(self, node):
        return 'int'

    def visit_Float(self, node):
        return 'float'

    def visit_String(self, node):
        return 'string'

    def visit_Id(self, node):
        if symbolTable.get(node.const):
            return symbolTable.get(node.const).type
        else:
            return None

    def visit_BinaryExpression(self, node):
        type1 = self.visit(node.left)
        type2 = self.visit(node.right)
        op = node.op

        undeclared = False
        if type1 == 'id':
            type1 = symbolTable.get(node.left.const).type
            if not type1:
                errorString = "undeclared '%s'" % (node.left.id, )
                printError(node.left.lineno, errorString)
                undeclared = True
        if type2 == 'id':
            type2 = symbolTable.get(node.right.const).type
            if not type2:
                errorString = "undeclared '%s'" % (node.right.id, )
                printError(node.right.lineno, errorString)
                undeclared = True

        if undeclared:
            return None
        elif type1 in ttype[op] and type2 in ttype[op][type1]:
            return ttype[op][type1][type2]
        else:
            errorString = "unsupported operation: '%s' %s '%s'" % (type1, op, type2)
            printError(node.lineno, errorString)

    def visit_ParenthesisExpression(self, node):
        return self.visit(node.expression)

    def visit_FunctionCall(self, node):
        if len(node.instruction.list) == len(symbolTable.get(node.id).argsList):
            for i in xrange(len(node.instruction.list)):
                if self.visit(node.instruction.list[i]) != symbolTable.get(node.id).argsList[i].type:
                    errorString = "wrong parameter type '%s' - should be '%s'" % (self.visit(node.instruction.list[i]), symbolTable.get(node.id).argsList[i].type)
                    printError(node.lineno, errorString)
        else:
            errorString = "wrong number of parameters for function '%s'" % (node.id, )
            printError(node.lineno, errorString)
        return symbolTable.get(node.id).type

    def visit_Fundef(self, node):
        global symbolTable
        if symbolTable.put(str(node.id), FunctionSymbol(node.id, node.type, node.args_list.list)):
            errorString = "redeclaration of '%s'" % (node.id, )
            printError(node.lineno, errorString)
        symbolTable = SymbolTable(symbolTable, node.id.const)
        for arg in node.args_list.list:
            symbolTable.put(arg.id.const, VariableSymbol(arg.id, arg.type))
        self.visit(node.compound_instr)
        symbolTable = symbolTable.getParentScope()