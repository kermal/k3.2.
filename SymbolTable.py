#!/usr/bin/python
class VariableSymbol(object):
    def __init__(self, name, type):
        self.name = name
        self.type = type


class FunctionSymbol(object):
    def __init__(self, name, type, argsList):
        self.name = name
        self.type = type
        self.argsList = argsList

class SymbolTable(object):
    def __init__(self, parent, name):
        self.parent = parent
        self.name = name
        self.variables = {}

    def put(self, name, symbol):
        wasInMap = name in self.variables
        if not wasInMap:
            self.variables[name] = symbol

        return wasInMap

    def get(self, name):
        if name in self.variables:
            return self.variables[name]
        else:
            if self.getParentScope():
                return self.getParentScope().get(name)
            else:
                return None

    def getParentScope(self):
        return self.parent





